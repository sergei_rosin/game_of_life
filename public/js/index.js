import vars from './vars.js';
import handlers from './handlers.js';
import { Grid } from './grid.js';

const updateHeaders = ({ generationTimes, drawingTimes, nextGenerationsAmount }) => {
	document.getElementById('generationNumber').innerText = `New generations: ${nextGenerationsAmount}`;
	updateTime('averageGenerationTime', 'minGenerationTime', 'maxGenerationTime', generationTimes);
	updateTime('averageDrawingTime', 'minDrawingTime', 'maxDrawingTime', drawingTimes);
};

const updateTime = (avarageEl, minEl, maxEl, times) => {
	document.getElementById(avarageEl).innerText = `Average: ${(times.reduce((a, b) => a + b, 0) / times.length) || '---'}`;
	const max = Math.max(...times);
	const min = Math.min(...times);
	document.getElementById(maxEl).innerText = `Max: ${isFinite(max) ? max : '---'}`;
	document.getElementById(minEl).innerText = `Min: ${isFinite(min) ? min : '---'}`;
};

const mutation = () => {
	if (vars.generations !== 0 && vars.grid.nextGenerationsAmount === vars.generations) {
		stopEvolve();
		return;
	} else if (!vars.grid.hasAliveCells()) {
		alert('No more dead cells');
		stopEvolve();
		return;
	}
	vars.grid.mutate();
	vars.grid.draw();
	updateHeaders(vars.grid);
	vars.ongoingProccesses.push(setTimeout(() => {
		requestAnimationFrame(() => mutation(vars.grid));
	}, 1000 / vars.frames));
};

window.startMutation = () => {
	if (!vars.grid.hasAliveCells()) {
		alert('No alive cells, no point of starting');
		return;
	}
	if (vars.ongoingProccesses.length) return;
	const newRows = Number(document.getElementById('rows').value);
	const newColumns = Number(document.getElementById('columns').value);
	if (vars.rows !== newRows || vars.columns !== newColumns) {
		vars.rows = newRows;
		vars.columns = newColumns;
		vars.grid.reinit();
	}
	vars.generations = Number(document.getElementById('generations').value);
	mutation(vars.grid);
};

window.stopEvolve = () => {
	if (vars.ongoingProccesses.length) {
		vars.ongoingProccesses.map(p => clearTimeout(p));
		vars.ongoingProccesses = [];
	}
};

window.clearBoard = () => {
	stopEvolve();
	vars.grid = new Grid(false);
	updateHeaders(vars.grid);
	vars.grid.draw();
};

window.generateRandomGrid = () => {
	clearBoard();
	vars.rows = Number(document.getElementById('rows').value);
	vars.columns = Number(document.getElementById('columns').value);
	vars.generations = Number(document.getElementById('generations').value);
	vars.grid = new Grid();
	vars.grid.draw();
};

window.switchShowGrid = () => {
	vars.showGrid = !vars.showGrid;
	if (!vars.ongoingProccesses.length) vars.grid.draw();
};

window.onload = () => {
	vars.canvas = document.getElementById('canvas');
	vars.canvas.height = vars.fieldSize;
	vars.canvas.width = vars.fieldSize;
	vars.canvas.addEventListener('mousemove', handlers.mouseMoveHandler, false);
	vars.canvas.addEventListener('mousedown', handlers.mouseDownHandler, false);
	vars.canvas.addEventListener('mouseup', handlers.mouseUpHandler, false);
	vars.ctx = vars.canvas.getContext('2d');
	vars.ctx.fillStyle = "#000000";
	vars.ctx.strokeStyle = "#D3D3D3";
	document.getElementById('rows').value = vars.rows;
	document.getElementById('columns').value = vars.columns;
	document.getElementById('generations').value = vars.generations;
	if (vars.showGrid) document.getElementById('showGrid').setAttribute('checked', true);
	vars.grid = new Grid(false);
	vars.grid.draw();
};