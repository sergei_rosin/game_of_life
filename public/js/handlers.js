import vars from './vars.js';

let mouseDown = false;
let cellsChangedWhileDrawing = [];

const mainHandler = (e) => {
	if (vars.ongoingProccesses.length) {
		mouseDown = false;
		alert('No drawing while generation in proccess');
		return;
	}
	const x = (e.pageX || e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft) - canvas.offsetLeft;
	const y = (e.pageY || e.clientY + document.body.scrollTop + document.documentElement.scrollTop) - canvas.offsetTop;
	const i = Math.floor(x / vars.cellSize()), j = Math.floor(y / vars.cellSize());
	let cell;
	try { cell = vars.grid.cells[i][j]; } catch (error) { return; }
	const wasCellChanged = cellsChangedWhileDrawing.filter((c) => c === cell)[0] !== undefined;
	if (!wasCellChanged) {
		cell.toggleLive();
		vars.grid.drawCell(cell, true);
		cellsChangedWhileDrawing.push(cell);
	}
};

export default {
	mouseDownHandler: (e) => {
		mouseDown = true;
		mainHandler(e);
	},
	mouseUpHandler: (e) => {
		mouseDown = false;
		cellsChangedWhileDrawing = [];
	},
	mouseMoveHandler: (e) => {
		if (mouseDown) { mainHandler(e); }
	}
};

