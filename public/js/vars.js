class Vars {
	constructor() {
		this.rows = 200;
		this.columns = 200;
		this.frames = 10;
		this.generations = 0;
		this.fieldSize = 1000;
		this.showGrid = true;
		this.ongoingProccesses = [];
		this.canvas = undefined;
		this.ctx = undefined;
		this.grid = undefined;
	}
	cellSize = () => this.rows >= this.columns ? this.fieldSize / this.rows : this.fieldSize / this.columns;
}
export default new Vars();