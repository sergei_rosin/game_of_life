import vars from './vars.js';

export class Cell {
	constructor(col, row, alive) {
		this.col = col;
		this.row = row;
		this.alive = alive;
		this.shouldDie = false;
		this.shouldLive = false;
		this.neighbours = [];

	}

	live() { this.alive = true; this.shouldLive = false; }

	toggleLive() { this.alive = !this.alive; this.shouldLive = false; this.shouldDie = false; }

	die() { this.alive = false; this.shouldDie = false; }

	getAliveNeighboursCount() {
		let amount = 0;
		for (let i = 0, l = this.neighbours.length; i < l; i++) {
			if (this.neighbours[i].alive) amount++;
		}
		return amount;
	}
}

export class Grid {
	constructor(randomize) {
		this.cells = [];
		for (let i = 0; i < vars.columns; i++) {
			this.cells[i] = [];
			for (let j = 0; j < vars.rows; j++) {
				const alive = randomize === undefined ? Math.random() > 0.7 : undefined;
				this.cells[i][j] = new Cell(i, j, alive);
			}
		}
		this.setNeighbours();
		this.generationTimes = [];
		this.drawingTimes = [];
		this.nextGenerationsAmount = 0;
	}

	setNeighbours() {
		this.iterateThroughCells((cell) => {
			cell.neighbours = [];
			for (let i = -1; i < 2; i++) {
				for (let j = -1; j < 2; j++) {
					if (i == 0 && j == 0) continue;
					const col = (cell.col + i + vars.columns) % vars.columns;
					const row = (cell.row + j + vars.rows) % vars.rows;
					cell.neighbours.push(this.cells[col][row]);
				}
			}
		});
	}

	reinit() {
		const newCells = [];
		for (let i = 0; i < vars.columns; i++) {
			newCells[i] = [];
			for (let j = 0; j < vars.rows; j++) {
				newCells[i][j] = this.cells[i] && this.cells[i][j] ? this.cells[i][j] : new Cell(i, j);
			}
		}
		this.cells = newCells;
		this.setNeighbours();
	}

	mutate() {
		let t1 = performance.now();
		const cellsToChange = [];
		this.iterateThroughCells((cell) => {
			const neighbours = cell.getAliveNeighboursCount();
			if (!cell.alive && neighbours === 3) {
				cell.shouldLive = true;
				cellsToChange.push(cell);
			} else if (cell.alive && (neighbours < 2 || neighbours > 3)) {
				cell.shouldDie = true;
				cellsToChange.push(cell);
			}
		});
		for (let i = 0, l = cellsToChange.length; i < l; i++) {
			const cell = cellsToChange[i];
			if (cell.shouldDie) { cell.die(); continue; }
			if (cell.shouldLive) { cell.live(); continue; }
		}
		this.generationTimes.push(performance.now() - t1);
		this.nextGenerationsAmount++;
	}

	iterateThroughCells(callback) {
		for (let i = 0, len = this.cells.length; i < len; i++) {
			for (let j = 0, len2 = this.cells[i].length; j < len2; j++) {
				callback(this.cells[i][j], i, j);
			}
		}
	}

	hasAliveCells() {
		const filtered = [];
		this.iterateThroughCells((c) => { if (c.alive) filtered.push(c); });
		return filtered.length > 0;
	}

	drawCellGrid(cell) {
		if (!vars.showGrid) return;
		vars.ctx.strokeRect(
			cell.col * vars.cellSize(),
			cell.row * vars.cellSize(),
			vars.cellSize(),
			vars.cellSize(),
		);
	}

	drawCell(cell, clear) {
		if (cell.alive) {
			vars.ctx.fillRect(
				cell.col * vars.cellSize(),
				cell.row * vars.cellSize(),
				vars.cellSize(),
				vars.cellSize(),
			);
		} else if (clear) {
			vars.ctx.clearRect(
				cell.col * vars.cellSize(),
				cell.row * vars.cellSize(),
				vars.cellSize(),
				vars.cellSize()
			);
		}
		this.drawCellGrid(cell);
	}

	draw() {
		let t1 = performance.now();
		vars.ctx.clearRect(0, 0, vars.fieldSize, vars.fieldSize);
		this.iterateThroughCells((cell) => this.drawCell(cell));
		this.drawingTimes.push(performance.now() - t1);
	}
}
